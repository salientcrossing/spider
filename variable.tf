
variable "private_subnet_cidr" {
  type    = list(any)
  default = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24", "10.0.7.0/24"]
}

variable "aws_vpc" {
  type    = list(any)
  default = ["vpc-10-1", "vpc-10-2"]
}

variable "aws_cidr" {
  type = map(any)
  default = {
    vpc-10-1 = "10.0.0.0/16"
    # "vpc-devt-proja" = "10.3.0.0/16"
    # "vpc-10-2"       = "10.2.0.0/16"
    # "vpc-devt-projx" = "10.4.0.0/16"
  }
}

variable "public_subnet_cidr" {
  type    = list(any)
  default = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]

}

variable "ecs_name" {
  type    = string
  default = "ecs-cluster"
}