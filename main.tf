
resource "aws_vpc" "VPC" {
  #   count                            = var.mycount
  assign_generated_ipv6_cidr_block = false
  cidr_block                       = var.aws_cidr.vpc-10-1
  enable_dns_hostnames             = false
  enable_dns_support               = true
  tags = {
    Name = "vpc_${var.aws_cidr.vpc-10-1}"
  }
}

resource "aws_internet_gateway" "myigw" {
  vpc_id = aws_vpc.VPC.id

  tags = {
    Name = "myigw"
  }
}

resource "aws_subnet" "myprivsubnet" {
  count = length(var.private_subnet_cidr)
    

  vpc_id            = aws_vpc.VPC.id
  availability_zone = element(slice(data.aws_availability_zones.az.names, 0, 3), count.index)
  cidr_block        = var.private_subnet_cidr[count.index]
  tags = {
    Name = "Priv_subnet_${var.private_subnet_cidr[count.index]}"
  }
}

# resource "aws_subnet" "mypubsubnet" {
#   count = length(var.public_subnet_cidr)

#   availability_zone       = element(slice(data.aws_availability_zones.az.names, 0, 3), count.index)
#   cidr_block              = var.public_subnet_cidr[count.index]
#   map_public_ip_on_launch = true
#   tags = {
#     Name = "Pub_subnet_${var.public_subnet_cidr[count.index]}"
#   }
#   vpc_id = aws_vpc.VPC.id

# }

# # Public Route Table 
# resource "aws_route_table" "public_route_table" {
#   vpc_id = aws_vpc.VPC.id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.myigw.id
#   }

#   tags = {
#     Name = "public_route_table"
#   }
# }

# #  Route table association
# resource "aws_route_table_association" "a" {
#   count = length(var.public_subnet_cidr)

#   subnet_id      = element(aws_subnet.mypubsubnet[*].id, count.index)
#   route_table_id = aws_route_table.public_route_table.id
# }

# # Private Route Table and Dependencies

# resource "aws_eip" "nat_eip" {
#   count = length(var.private_subnet_cidr)

#   vpc = true

#   tags = {
#     Name = "nat_eip_subnet_${var.private_subnet_cidr[count.index]}"
#   }
# }

# resource "aws_nat_gateway" "nat_gateway" {
#   count = length(var.private_subnet_cidr)


#   allocation_id = element(aws_eip.nat_eip[*].id, count.index)
#   subnet_id     = element(aws_subnet.mypubsubnet[*].id, count.index)

#   tags = {
#     Name = "nat_gateway_${var.private_subnet_cidr[count.index]}"

#   }
#   depends_on = [aws_internet_gateway.myigw]
# }

# resource "aws_route_table" "private_route_table" {
#   count  = length(var.private_subnet_cidr)
#   vpc_id = aws_vpc.VPC.id

#   route {
#     nat_gateway_id = element(aws_nat_gateway.nat_gateway[*].id, count.index)

#   }

#   tags = {
#     "Name" = "private_route_table_${var.private_subnet_cidr[count.index]}"
#   }

# }

# # Configuration section for route table association on application route table 
# resource "aws_route_table_association" "b" {
#   count = length(var.private_subnet_cidr)

#   subnet_id      = element(aws_subnet.myprivsubnet[*].id, count.index)
#   route_table_id = element(aws_route_table.private_route_table[*].id, count.index)
# }
