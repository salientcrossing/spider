resource "aws_ecs_task_definition" "client" {
  family                   = "${var.ecs_name}_family"
  requires_compatibilities = ["FARGATE"]
  memory                   = 512
  cpu                      = 256
  network_mode             = "awsvpc"

  container_definitions = jsonencode([
    {
      name      = "client"
      image     = "ncholasjacksin/fake-service:v0.23.1" #docker image
      cpu       = 0
      essential = true

      portMapping = [                         # connecting host port to container port it is just making sure that from the outside we are gonna know what port it exposes
        {
          containerPort = 9090                        
          hostPort      = 9090
          protocol      = "tcp"

        }
      ]
      environment = [
        {
            name = "Name"
            value = "client"
        },
        {
            name = "MESSAGE"
            value = "Hello from the client"
        }
      ]

    }
  ])

}