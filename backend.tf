terraform {
  backend "s3" {
    bucket         = "silvercrossing"
    key            = "myenaj.tfstate"
    dynamodb_table = "enaj-DB"
    region         = "us-east-1"
  }
}